<article <?php post_class( 'qodef-blog-item qodef-e' ); ?>>
    <div class="qodef-e-inner">
		<?php
		// Include post media
		mildhill_template_part( 'blog', 'templates/parts/post-info/media' );
		?>
        <div class="qodef-e-content">
            <div class="qodef-e-info qodef-info--top">
				<?php
				// Include post category and date info
				mildhill_template_part( 'blog', 'templates/parts/post-info/category-and-date' );
				?>
            </div>
            <div class="qodef-e-text">
				<?php
				// Include post title
				mildhill_template_part( 'blog', 'templates/parts/post-info/title' );

				// Include post excerpt
				mildhill_template_part( 'blog', 'templates/parts/post-info/excerpt' );

				// Hook to include additional content after blog single content
				do_action( 'mildhill_action_after_blog_single_content' );
				?>
            </div>
            <div class="qodef-e-info qodef-info--bottom">
				<?php
				// Include post read more
				mildhill_template_part( 'blog', 'templates/parts/post-info/read-more' );
				?>
            </div>
        </div>
    </div>
</article>