<div id="qodef-page-content-bottom">
	<?php
	// Hook to include additional content before page content bottom content
	do_action( 'mildhill_action_before_page_content_bottom_content' );

	// Include module content template
	echo apply_filters( 'mildhill_filter_content_bottom_content_template', mildhill_get_template_part( 'content-bottom', 'templates/content-bottom-content' ) );

	// Hook to include additional content after page content bottom  content
	do_action( 'mildhill_action_after_page_content_bottom_content' );
	?>
</div>