(function ($) {
    "use strict";

    $(document).ready(function(){

        $(document).on('change', '.dev-quantity-input', function(){
            var thisVal  = parseInt( $(this).val() );

            thisVal = ( thisVal > 0 ) ? thisVal : 1;

            $(this).closest('.qodef-woo-buttons-holder').find('.add_to_cart_button').attr('data-quantity', thisVal);

            console.log( thisVal );
        });

        $(document).on('click', '.qodef-widget-title', function(){

            $(this).closest('.widget').toggleClass('active');
        });

    });

})(jQuery);


// Infinite Scroll for Shop
(function($){
    'use strict';

    $(document).ready(function(){

        var flag = false;
        var main_xhr;
        var ajax_loader = '<div class="sblfwc-loader shop"><div class="lds-dual-ring"></div></div>';
        $('.qodef-woo-product-list').append(ajax_loader);

        var amPushAjax = function( url, className ){
            if(main_xhr && main_xhr.readyState != 4){
                main_xhr.abort();
            }
            main_xhr = $.ajax({
                url: url,
                asynch: true,
                beforeSend: function() {
                    $('.sblfwc-loader').show();
                    flag = true;
                },
                success: function(data) {
                    $('.sblfwc-loader').hide();

                    $('.woocommerce-pagination').html($('.woocommerce-pagination', data).html());
                    $('.qodef-woo-product-list .products').append($('.qodef-woo-product-list .products', data).html());

                    document.title = $(data).filter('title').text();

                    flag = false;

                }
            });
        };

        // Shop Ajax Trigger
        $( document ).on('click', '.woocommerce-pagination a.next.page-numbers', function(e){
            e.preventDefault();

            var targetUrl = ( e.target.href ) ? e.target.href : $(this).context.href;
            amPushAjax( targetUrl, 'feed' );
            window.history.pushState({url: "" + targetUrl + ""}, "", targetUrl);
        });
        // End Shop Ajax Trigger

        // Shop Click Trigger
        $( window ).on('scroll', function(e){
            $('.woocommerce-pagination a.next.page-numbers').each(function(i,el){

                var $this = $(this);

                var H = $(window).height(),
                    r = el.getBoundingClientRect(),
                    t=r.top,
                    b=r.bottom;

                //var tAdj = parseInt(t-(H+(H/2)));
                var tAdj = parseInt(t-H);

                //console.log( H, t,tAdj );

                if ( flag === false && (H >= tAdj) ) {
                    //console.log( 'inview' );
                    $this.trigger('click');
                } else {
                    //console.log( 'outview' );
                }
            });
        });
        // End Shop Click Trigger

    });


})(jQuery);