<?php

// Include mobile navigation opener
mildhill_template_part( 'mobile-header', 'templates/parts/mobile-navigation-opener' );

// Include mobile logo
mildhill_template_part( 'mobile-header', 'templates/parts/mobile-logo' );

// Include mobile navigation
mildhill_template_part( 'mobile-header', 'templates/parts/mobile-navigation' );