<div id="qodef-page-comments">
	<?php if ( have_comments() ) {
		$comments_number = get_comments_number();
		?>
        <div id="qodef-page-comments-list" class="qodef-m">
            <h3 class="qodef-m-title">
				<?php echo sprintf( esc_html__( 'Comments: %s', 'mildhill' ), $comments_number ); ?>
            </h3>
            <ul class="qodef-m-comments">
				<?php wp_list_comments( array_unique( array_merge( array( 'callback' => 'mildhill_get_comments_list_template' ), apply_filters( 'mildhill_filter_comments_list_template_callback', array() ) ) ) ); ?>
            </ul>

			<?php if ( get_comment_pages_count() > 1 ) { ?>
                <div class="qodef-m-pagination qodef--wp">
					<?php the_comments_pagination( array(
						'prev_text'          => mildhill_get_icon( 'arrow_carrot-left', 'elegant-icons', esc_html__( '<', 'mildhill' ) ),
						'next_text'          => mildhill_get_icon( 'arrow_carrot-right', 'elegant-icons', esc_html__( '>', 'mildhill' ) ),
						'before_page_number' => '0'
					) ); ?>
                </div>
			<?php } ?>
        </div>
	<?php } ?>
	<?php if ( ! comments_open() && get_comments_number() && post_type_supports( get_post_type(), 'comments' ) ) { ?>
        <p class="qodef-page-comments-not-found"><?php esc_html_e( 'Comments are closed.', 'mildhill' ); ?></p>
	<?php } ?>

    <div id="qodef-page-comments-form">
		<?php comment_form( apply_filters( 'mildhill_filter_comment_form_args', array() ) ); ?>
    </div>
</div>