<?php
$tags = get_the_tags();

if ( $tags ) { ?>
    <div class="qodef-e-info-item qodef-e-info-tags">
        <div class="qodef-e-info-tags-title">
			<?php esc_html_e( 'Tags:', 'mildhill' ) ?>
        </div>
		<?php the_tags( '', ', ', '' ); ?>
    </div>
<?php } ?>