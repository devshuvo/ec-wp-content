<div class="qodef-e-media">
	<?php switch ( get_post_format() ) {
		case 'gallery':
			mildhill_template_part( 'blog', 'templates/parts/post-format/gallery' );
			break;
		case 'video':
			mildhill_template_part( 'blog', 'templates/parts/post-format/video' );
			break;
		case 'audio':
			mildhill_template_part( 'blog', 'templates/parts/post-format/audio' );
			break;
		default:
			mildhill_template_part( 'blog', 'templates/parts/post-info/image' );
			break;
	} ?>
</div>