<?php
/**
 * Plugin Name: Search by list for WooCommerce
 * Plugin URI:
 * Github Plugin URI:
 * Description:
 * Author:
 * Author URI:
 * Version: 1.0.1005
 * Text Domain: sblfwc
 * Domain Path: /lang
 */


/**
* Including Plugin file for security
* Include_once
*
* @since 1.0.0
*/
include_once( ABSPATH . 'wp-admin/includes/plugin.php' );

/**
 *	Helper Function
 */
require_once( dirname( __FILE__ ) . '/includes/helper-functions.php' );

/**
 *	SVG ICON
 */
require_once( dirname( __FILE__ ) . '/includes/svg-icons.php' );


/**
 *	SearchByListForWooCommerce Plugin Class
 */
if ( ! class_exists( 'SearchByListForWooCommerce' ) ) :
	class SearchByListForWooCommerce{

		public function __construct() {
			// Loaded textdomain
			add_action('plugins_loaded', array( $this, 'plugin_loaded_action' ), 10, 2);
			// Enqueue frontend scripts
			add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_frontend_script' ), 15 );
			// Enqueue admin scripts
			//add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_admin_script' ), 100 );
			add_shortcode( 'sblfwoo_list', array( $this, 'list_shortcode' ) );
			add_shortcode( 'sblfwoo_result', array( $this, 'result_shortcode' ) );

			add_action( 'wp_ajax_search_list_product_filter', array( $this, 'search_list_product_filter_ajax_function' ) );
			add_action( 'wp_ajax_nopriv_search_list_product_filter', array( $this, 'search_list_product_filter_ajax_function' ) );

		}

		/**
		* Loading Text Domain for Internationalization
		*
		*  @return void
		*
		*/
		function plugin_loaded_action() {
			load_plugin_textdomain( 'sblfwc', false, dirname( plugin_basename(__FILE__) ) . '/lang/' );
		}

		/**
		 * Enqueue frontend script
		 *
		 *  @return void
		 *
		 */
		function enqueue_frontend_script() {

			$ver = current_time('timestamp');

		    wp_enqueue_style( 'searchbylistforwoocommerce', plugin_dir_url( __FILE__ ) . 'assets/css/sblfwc-styles.css', array(), $ver );

		    wp_enqueue_script( 'searchbylistforwoocommerce', plugin_dir_url( __FILE__ ) . 'assets/js/sblfwc-scripts.js', array('jquery'), $ver );

		    wp_localize_script( 'searchbylistforwoocommerce', 'sblfwc_params', array(
		        'sblfwc_ajax_nonce' => wp_create_nonce( 'sblfwc_ajax_nonce' ),
		        'ajax_url' => admin_url( 'admin-ajax.php' ),
		        'sl_url' => esc_url( home_url('/search-by-list-result/') ),
		    ));
		}

		/**
		 * Enqueue aadmin script
		 *
		 * @return void
		 *
		 */
		function enqueue_admin_script( $hook ) {

		    //wp_enqueue_style( 'cv19-solutions-admin', plugin_dir_url( __FILE__ ) . 'assets/admin/css/cv19-solutions-admin.min.css' );

		    //wp_enqueue_script( 'cv19-solutions-admin', plugin_dir_url( __FILE__ ) . 'assets/admin/js/cv19-solutions-admin.js', array('jquery'), '1.0' );
		}

		/**
		 * Add to List Shortcode function
		 */
		function list_shortcode( $atts ){
			ob_start(); ?>
			<div class="sblfwoo-wrap">
				<div class="sblfwoo-inner">
					<a href="#" class="sblfwoo-trigger">
						<div class="icon-text">
							<span>Activa la</span>
							<span>búsqueda</span>
							<span>por lista</span>
						</div>
						<?php sblfwoo_svg('list-icon-img'); ?>
					</a>
					<div class="sblfwoo-container">
						<?php do_action( 'sblfwoo_container_start' ); ?>
						<div class="input-container-text">Ingrese todos los productos de tu lista separados por una coma <span class="sep"></span></div>
						<a class="sblfwoo-close">X</a>
						<div class="sblfwoo-container-inner">
							<?php do_action( 'sblfwoo_container_inner_start' ); ?>
							<div class="input-top-text">Ejemplo: Avena, maca, papaya</div>

							<div class="search-product-list__input-wrapper">
							    <div class="search-product-list__list-items">
							    	<input type="text" class="search-product-list__search-input" id="search-list-item-input" value="" size="0" />
							    </div>
							</div>

							<?php do_action( 'sblfwoo_container_inner_end' ); ?>
						</div>
						<a href="#" class="sblfwc-submit"><?php esc_html_e( 'Buscar' );?></a>
						<?php do_action( 'sblfwoo_container_end' ); ?>
					</div>
				</div>
			</div>
			<?php return ob_get_clean();
		}

		/**
		 * Result Shortcode
		 */
		function result_shortcode( $atts ){
			if( isset( $_GET['sl'] ) && $_GET['sl'] != "" ){

				$search_lists = explode( ',', $_GET['sl'] );
				$search_lists = array_unique( $search_lists );

			} else {
				wp_safe_redirect( home_url() );
				return;
			}

			ob_start();

			$args = array(
				'post_type' => 'product',
				'posts_per_page' => -1,
				//'s' => 'tea',
			);

			?>
			<div class="search-list-filters-wrapper hide-controls">
			    <div class="search-list-filters">
			    	<?php foreach ( $search_lists as $key => $value ) : ?>
			    		<?php
			    		$args['s'] = sanitize_text_field( $value );
						$query = new WP_Query( $args );
						$total = $query->found_posts;
						wp_reset_postdata();
			    		?>
			    		<button type="button" class="search-filter <?php sblfwc_activated( $key, 0, true );?> " data-filter="<?php esc_attr_e( $value ); ?>"><span class="text"><?php esc_html_e( $value ); ?></span><span class="amount">(<?php esc_html_e( $total );; ?>)</span></button>
			    	<?php endforeach; ?>

			    </div>
			</div>

			<div id="search-list-filter-ajax-result">
				<?php
				$args['s'] = $search_lists[0];
				$the_query = new WP_Query( $args );
				/**
				 * Hook: woocommerce_before_main_content.
				 *
				 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
				 * @hooked woocommerce_breadcrumb - 20
				 * @hooked WC_Structured_Data::generate_website_data() - 30
				 */
				do_action( 'woocommerce_before_main_content' );
				?>

				<?php if( $the_query->have_posts() ) : ?>
					<?php woocommerce_product_loop_start(); ?>
					<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
						<?php do_action( 'woocommerce_shop_loop' ); ?>
						<?php wc_get_template_part( 'content', 'product' ); ?>

					<?php endwhile; ?>

					<?php woocommerce_product_loop_end();
					/**
					 * Hook: woocommerce_after_shop_loop.
					 *
					 * @hooked woocommerce_pagination - 10
					 */
					do_action( 'woocommerce_after_shop_loop' ); ?>

					<?php wp_reset_query(); ?>

				<?php else : ?>
					<?php
					/**
					 * Hook: woocommerce_no_products_found.
					 *
					 * @hooked wc_no_products_found - 10
					 */
					do_action( 'woocommerce_no_products_found' );
					?>

				<?php endif; ?>

				<?php
				/**
				 * Hook: woocommerce_after_main_content.
				 *
				 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
				 */
				do_action( 'woocommerce_after_main_content' );
				?>
			</div>

			<?php return ob_get_clean();
		}

		/**
		 * Ajax result
		 */
		function search_list_product_filter_ajax_function(){
			// Verify nonce
		  	if( !isset( $_POST['sblfwc_ajax_nonce'] ) || !wp_verify_nonce( $_POST['sblfwc_ajax_nonce'], 'sblfwc_ajax_nonce' ) )
		    die('Permission denied');

			$data_filter = sanitize_text_field( $_POST['data_filter'] );

			$args = array(
				'post_type' => 'product',
				'posts_per_page' => -1,
				's' => $data_filter,
			);

			$the_query = new WP_Query( $args );

			ob_start();

			/**
			 * Hook: woocommerce_before_main_content.
			 *
			 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
			 * @hooked woocommerce_breadcrumb - 20
			 * @hooked WC_Structured_Data::generate_website_data() - 30
			 */
			do_action( 'woocommerce_before_main_content' );
			?>

			<?php if( $the_query->have_posts() ) : ?>
				<?php woocommerce_product_loop_start(); ?>
				<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
					<?php do_action( 'woocommerce_shop_loop' ); ?>
					<?php wc_get_template_part( 'content', 'product' ); ?>

				<?php endwhile; ?>

				<?php woocommerce_product_loop_end();
				/**
				 * Hook: woocommerce_after_shop_loop.
				 *
				 * @hooked woocommerce_pagination - 10
				 */
				do_action( 'woocommerce_after_shop_loop' ); ?>

				<?php wp_reset_query(); ?>

			<?php else : ?>
				<?php
				/**
				 * Hook: woocommerce_no_products_found.
				 *
				 * @hooked wc_no_products_found - 10
				 */
				do_action( 'woocommerce_no_products_found' );
				?>

			<?php endif; ?>

			<?php
			/**
			 * Hook: woocommerce_after_main_content.
			 *
			 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
			 */
			do_action( 'woocommerce_after_main_content' );

			$output = ob_get_clean();

			echo $output;

			die();
		}

	}

endif;

new SearchByListForWooCommerce();
