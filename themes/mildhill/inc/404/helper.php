<?php

if ( ! function_exists( 'mildhill_set_404_page_inner_classes' ) ) {
	/**
	 * Function that return classes for the page inner div from header.php
	 *
	 * @param $classes string
	 *
	 * @return string
	 */
	function mildhill_set_404_page_inner_classes( $classes ) {

		if ( is_404() ) {
			$classes = 'qodef-content-full-width';
		}

		return $classes;
	}

	add_filter( 'mildhill_filter_page_inner_classes', 'mildhill_set_404_page_inner_classes' );
}

if ( ! function_exists( 'mildhill_get_404_page_parameters' ) ) {
	/**
	 * Function that set 404 page area content parameters
	 */
	function mildhill_get_404_page_parameters() {

		$params = array(
			'title'       => esc_html__( '404 Error Page', 'mildhill' ),
			'text'        => esc_html__( 'The page you are looking for doesn\'t exist. It may have been moved or removed altogether. Please try searching for some other page, or return to the site\'s homepage to find what you\'re looking for.', 'mildhill' ),
			'button_text' => esc_html__( 'Back to home', 'mildhill' )
		);

		return apply_filters( 'mildhill_filter_404_page_template_params', $params );
	}
}