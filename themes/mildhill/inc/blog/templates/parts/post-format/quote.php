<?php
$quote_meta           = get_post_meta( get_the_ID(), 'qodef_post_format_quote_text', true );
$quote_text           = ! empty( $quote_meta ) ? $quote_meta : get_the_title();

if ( ! empty( $quote_text ) ) :
	$quote_author = get_post_meta( get_the_ID(), 'qodef_post_format_quote_author', true );
	$title_tag        = isset( $title_tag ) && ! empty( $title_tag ) ? $title_tag : 'p';
	$author_title_tag = isset( $author_title_tag ) && ! empty( $author_title_tag ) ? $author_title_tag : 'var';
	?>
    <div class="qodef-e-quote">

    <div class="qodef-e-icon">
		<?php mildhill_render_icon( 'icon_quotations', 'elegant-icons', '' ); ?>
    </div>

    <div class="qodef-e-info qodef-info--top">
		<?php
		// Include post category and date info
		mildhill_template_part( 'blog', 'templates/parts/post-info/category-and-date' );
		?>
    </div>

	<?php /*if ( ! is_single() ) : */
	?>
    <a itemprop="url" class="qodef-e-quote-url" href="<?php the_permalink(); ?>"
       title="<?php the_title_attribute(); ?>">
		<?php echo '<' . esc_attr( $title_tag ); ?> class="qodef-e-quote-text">
		<?php echo esc_html( $quote_text ); ?>
		<?php echo '</' . esc_attr( $title_tag ); ?>>
    </a>
	<?php /*endif; */
	?>

	<?php if ( ! empty( $quote_author ) ) : ?>
	<?php echo '<' . esc_attr( $author_title_tag ); ?> class="qodef-e-quote-author"><?php echo esc_html( $quote_author ); ?></<?php echo esc_attr( $author_title_tag ); ?>>
<?php endif; ?>

    <div class="qodef-e-quote-media">
		<?php mildhill_template_part( 'blog', 'templates/parts/post-info/svg-holder' ); ?>
		<?php mildhill_template_part( 'blog', 'templates/parts/post-info/image', 'background' ); ?>
    </div>
    </div>
<?php endif; ?>