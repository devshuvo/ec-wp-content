<?php

define( 'MILDHILL_ROOT', get_template_directory_uri() );
define( 'MILDHILL_ROOT_DIR', get_template_directory() );
define( 'MILDHILL_ASSETS_ROOT', MILDHILL_ROOT . '/assets' );
define( 'MILDHILL_ASSETS_ROOT_DIR', MILDHILL_ROOT_DIR . '/assets' );
define( 'MILDHILL_ASSETS_CSS_ROOT', MILDHILL_ASSETS_ROOT . '/css' );
define( 'MILDHILL_ASSETS_CSS_ROOT_DIR', MILDHILL_ASSETS_ROOT_DIR . '/css' );
define( 'MILDHILL_ASSETS_JS_ROOT', MILDHILL_ASSETS_ROOT . '/js' );
define( 'MILDHILL_ASSETS_JS_ROOT_DIR', MILDHILL_ASSETS_ROOT_DIR . '/js' );
define( 'MILDHILL_INC_ROOT', MILDHILL_ROOT . '/inc' );
define( 'MILDHILL_INC_ROOT_DIR', MILDHILL_ROOT_DIR . '/inc' );
