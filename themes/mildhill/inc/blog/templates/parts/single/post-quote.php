<article <?php post_class( 'qodef-blog-item qodef-e' ); ?>>
    <div class="qodef-e-inner">
		<?php
		// Include post format part
		mildhill_template_part( 'blog', 'templates/parts/post-format/quote' ); ?>
        <div class="qodef-e-content">
            <div class="qodef-e-text">
				<?php
				// Include post content
				the_content();

				// Hook to include additional content after blog single content
				do_action( 'mildhill_action_after_blog_single_content' );
				?>
            </div>
            <div class="qodef-e-info qodef-info--bottom">
                <div class="qodef-e-info-left">
					<?php
					// Include post tags info
					mildhill_template_part( 'blog', 'templates/parts/post-info/tags' );
					?>
                </div>
				<?php if ( mildhill_is_installed( 'core' ) ): ?>
                    <div class="qodef-e-info-center">
						<?php
						// Include post share
						mildhill_template_part( 'blog', 'templates/parts/post-info/social-share' );
						?>
                    </div>
				<?php endif; ?>
                <div class="qodef-e-info-right">
					<?php
					// Include post comments info
					mildhill_template_part( 'blog', 'templates/parts/post-info/comments' );
					?>
                </div>
            </div>
        </div>
    </div>
</article>