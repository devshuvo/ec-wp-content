<div id="qodef-404-page">
	<?php

	$image = mildhill_is_installed( 'core' ) ? mildhill_core_get_post_value_through_levels( 'qodef_404_page_left_image' ) : '';

	if ( ! empty( $image ) ) {
		?>
        <div class="qodef-404-page-image">
			<?php
			echo wp_get_attachment_image( $image, 'full' ); ?>
        </div>
	<?php } ?>

    <div class="qodef-404-page-content">
        <h1 class="qodef-404-title"><?php echo esc_html( $title ); ?></h1>

        <p class="qodef-404-text "><?php echo esc_html( $text ); ?></p>

        <div class="qodef-404-button">
			<?php
			$button_params = array(
				'link' => esc_url( home_url( '/' ) ),
				'text' => esc_html( $button_text )
			);

			mildhill_render_button_element( $button_params ); ?>
        </div>
    </div>
</div>