<?php

if ( ! function_exists( 'mildhill_load_page_mobile_header' ) ) {
	/**
	 * Function which loads page template module
	 */
	function mildhill_load_page_mobile_header() {
		// Include mobile header template
		echo apply_filters( 'mildhill_filter_mobile_header_template', mildhill_get_template_part( 'mobile-header', 'templates/mobile-header' ) );
	}
	
	add_action( 'mildhill_action_page_header_template', 'mildhill_load_page_mobile_header' );
}

if ( ! function_exists( 'mildhill_register_mobile_navigation_menus' ) ) {
	/**
	 * Function which registers navigation menus
	 */
	function mildhill_register_mobile_navigation_menus() {
		$navigation_menus = apply_filters( 'mildhill_filter_register_mobile_navigation_menus', array( 'mobile-navigation' => esc_html__( 'Mobile Navigation', 'mildhill' ) ) );
		
		if ( ! empty( $navigation_menus ) ) {
			register_nav_menus( $navigation_menus );
		}
	}
	
	add_action( 'mildhill_action_after_include_modules', 'mildhill_register_mobile_navigation_menus' );
}