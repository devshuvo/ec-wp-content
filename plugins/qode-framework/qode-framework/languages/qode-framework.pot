# Copyright (C) 2018 Qode Themes
# This file is distributed under the same license as the Qode Framework plugin.
msgid ""
msgstr ""
"Project-Id-Version: Qode Framework 1.0\n"
"Report-Msgid-Bugs-To: https://wordpress.org/support/plugin/qode-framework\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"POT-Creation-Date: 2018-11-29T17:01:18+01:00\n"
"PO-Revision-Date: 2018-11-29T17:01:18+01:00\n"
"X-Generator: WP-CLI 2.0.1\n"
"X-Domain: qode-framework\n"

#. Plugin Name of the plugin
msgid "Qode Framework"
msgstr ""

#. Description of the plugin
msgid "Framework plugin developed as a base for Qode themes"
msgstr ""

#. Author of the plugin
msgid "Qode Themes"
msgstr ""

#: \inc\common\core\repeater-inner.php:22
#: \inc\common\core\repeater.php:22
msgid "Add New Item"
msgstr ""

#: \inc\common\fields\field-file.php:20
#: \inc\common\fields-wp\field-file.php:22
msgid "Select File"
msgstr ""

#: \inc\common\fields\field-file.php:20
#: \inc\common\fields\field-image.php:34
#: \inc\common\fields-wp\field-file.php:22
#: \inc\common\fields-wp\field-image.php:36
msgid "Upload"
msgstr ""

#: \inc\common\fields\field-file.php:21
#: \inc\common\fields\field-image.php:35
#: \inc\common\fields-wp\field-file.php:23
#: \inc\common\fields-wp\field-image.php:37
msgid "Remove"
msgstr ""

#: \inc\common\fields\field-iconpack.php:19
#: \inc\common\fields-nav-menu\field-iconpack.php:24
#: \inc\icons\icons.php:39
msgid "Select Icon Pack"
msgstr ""

#: \inc\common\fields\field-image.php:34
#: \inc\common\fields-wp\field-image.php:36
msgid "Select Image"
msgstr ""

#: \inc\common\modules\admin\core\options.php:15
msgid "Qode Options"
msgstr ""

#: \inc\common\modules\admin\core\options.php:174
msgid "Options reset to default"
msgstr ""

#: \inc\common\modules\admin\core\options.php:180
msgid "Saved"
msgstr ""

#: \inc\common\modules\admin\core\options.php:186
msgid "Wrong options trigger"
msgstr ""

#: \inc\common\modules\admin\templates\content.php:10
msgid "Save Changes"
msgstr ""

#: \inc\common\modules\nav-menu\core\page.php:36
msgid "Icon"
msgstr ""

#: \inc\fonts\helper.php:96
#: \inc\sidebar\custom-sidebar.php:171
msgid "Default"
msgstr ""

#: \inc\media\image-sizes.php:65
msgid "Max Width"
msgstr ""

#: \inc\media\image-sizes.php:68
msgid "Max Height"
msgstr ""

#: \inc\media\image-sizes.php:72
msgid "Crop to exact dimensions (normally are proportional) "
msgstr ""

#: \inc\post-types\custom-post-type-taxonomy.php:64
#: \inc\post-types\custom-post-type-taxonomy.php:74
msgid "%s %s"
msgstr ""

#: \inc\post-types\custom-post-type-taxonomy.php:65
msgid "Search %s %s"
msgstr ""

#: \inc\post-types\custom-post-type-taxonomy.php:66
msgid "All %s %s"
msgstr ""

#: \inc\post-types\custom-post-type-taxonomy.php:67
msgid "Parent %s %s"
msgstr ""

#: \inc\post-types\custom-post-type-taxonomy.php:68
msgid "Parent %s %s:"
msgstr ""

#: \inc\post-types\custom-post-type-taxonomy.php:69
msgid "Edit %s %s"
msgstr ""

#: \inc\post-types\custom-post-type-taxonomy.php:70
msgid "Update %s %s"
msgstr ""

#: \inc\post-types\custom-post-type-taxonomy.php:71
msgid "Add New %s %s"
msgstr ""

#: \inc\post-types\custom-post-type-taxonomy.php:72
msgid "New %s %s Name"
msgstr ""

#: \inc\post-types\custom-post-type-taxonomy.php:73
msgid "No %s %s Found"
msgstr ""

#: \inc\post-types\custom-post-type-taxonomy.php:91
#: \inc\post-types\custom-post-type.php:189
msgid "base"
msgstr ""

#: \inc\sidebar\custom-sidebar.php:14
msgid "Custom Sidebar"
msgstr ""

#: \inc\sidebar\custom-sidebar.php:47
msgid "This area allows you to add Custom widget area on your site"
msgstr ""

#: \inc\sidebar\custom-sidebar.php:49
msgid "Custom Sidebar Name"
msgstr ""

#: \inc\sidebar\custom-sidebar.php:50
msgid "Add Sidebar"
msgstr ""

#: \inc\sidebar\custom-sidebar.php:96
msgid "Custom sidebar is added"
msgstr ""

#: \inc\sidebar\custom-sidebar.php:98
#: \inc\sidebar\custom-sidebar.php:125
msgid "Nonce is invalid"
msgstr ""

#: \inc\sidebar\custom-sidebar.php:101
#: \inc\sidebar\custom-sidebar.php:128
msgid "POST is invalid"
msgstr ""

#: \inc\sidebar\custom-sidebar.php:120
msgid "Custom sidebar is deleted"
msgstr ""

#: \inc\sidebar\custom-sidebar.php:122
msgid "Custom sidebar name is invalid"
msgstr ""

#: \inc\widgets\qode-widget.php:232
msgid "There are no options for this widget."
msgstr ""
