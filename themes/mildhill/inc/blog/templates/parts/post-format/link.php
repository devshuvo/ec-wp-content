<?php
$link_url_meta = get_post_meta( get_the_ID(), 'qodef_post_format_link', true );

if ( mildhill_get_blog_holder_classes() === 'qodef--list' ) {
	$link_url = get_the_permalink();
} else {
	$link_url = ! empty( $link_url_meta ) ? $link_url_meta : get_the_permalink();
}

$link_text_meta = get_post_meta( get_the_ID(), 'qodef_post_format_link_text', true );

if ( ! empty( $link_url ) ) :
	$link_text = ! empty( $link_text_meta ) ? $link_text_meta : get_the_title();
	$title_tag  = isset( $title_tag ) && ! empty( $title_tag ) ? $title_tag : 'p';
	?>
    <div class="qodef-e-link">

        <div class="qodef-e-icon">
			<?php mildhill_render_icon( 'icon_link', 'elegant-icons', '' ); ?>
        </div>

        <div class="qodef-e-info qodef-info--top">
			<?php
			// Include post category and date info
			mildhill_template_part( 'blog', 'templates/parts/post-info/category-and-date' );
			?>
        </div>
        <a itemprop="url" class="qodef-e-link-url" href="<?php echo esc_url( $link_url ); ?>" target="_self">
			<?php echo '<' . esc_attr( $title_tag ); ?> class="qodef-e-link-text">
			<?php echo esc_html( $link_text ); ?>
			<?php echo '</' . esc_attr( $title_tag ); ?>>
        </a>

        <!--<a itemprop="url" class="qodef-e-link-url" href="<?php /*echo esc_url( $link_url ); */ ?>" target="_blank"></a>-->
        <div class="qodef-e-link-media">
			<?php mildhill_template_part( 'blog', 'templates/parts/post-info/svg-holder' ); ?>
			<?php mildhill_template_part( 'blog', 'templates/parts/post-info/image', 'background' ); ?>

        </div>
    </div>
<?php endif; ?>