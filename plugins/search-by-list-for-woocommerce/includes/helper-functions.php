<?php
function sblfwc_activated( $helper, $current, $echo ) {
    if ( (string) $helper === (string) $current ) {
        $result = " active";
    } else {
        $result = '';
    }

    if ( $echo ) {
        echo $result;
    }

    return $result;
}