<?php

if ( ! function_exists( 'mildhill_include_comments_in_templates' ) ) {
	/**
	 * Function which includes comments templates on pages/posts
	 */
	function mildhill_include_comments_in_templates() {
		// Include comments template
		comments_template();
	}

	add_action( 'mildhill_action_after_page_content', 'mildhill_include_comments_in_templates', 100 ); // permission 100 is set to comments template be at the last place
	add_action( 'mildhill_action_after_blog_post_item', 'mildhill_include_comments_in_templates', 100 );
}

if ( ! function_exists( 'mildhill_is_page_comments_enabled' ) ) {
	/**
	 * Function that check is module enabled
	 */
	function mildhill_is_page_comments_enabled() {
		$is_enabled = apply_filters( 'mildhill_filter_enable_page_comments', true );

		return $is_enabled;
	}
}

if ( ! function_exists( 'mildhill_load_page_comments' ) ) {
	/**
	 * Function which loads page template module
	 */
	function mildhill_load_page_comments() {
		if ( mildhill_is_page_comments_enabled() ) {
			mildhill_template_part( 'comments', 'templates/comments' );
		}
	}

	add_action( 'mildhill_action_page_comments_template', 'mildhill_load_page_comments' );
}

if ( ! function_exists( 'mildhill_get_comments_list_template' ) ) {
	/**
	 * Function which modify default wordpress comments list template
	 *
	 * @param $comment object
	 * @param $args array
	 * @param $depth int
	 *
	 * @return string that contains comments list html
	 */
	function mildhill_get_comments_list_template( $comment, $args, $depth ) {
		global $post;
		$GLOBALS['comment'] = $comment;

		$classes = array();

		$is_author_comment = $post->post_author == $comment->user_id;
		if ( $is_author_comment ) {
			$classes[] = 'qodef-comment--author';
		}

		$is_specific_comment = $comment->comment_type == 'pingback' || $comment->comment_type == 'trackback';
		if ( $is_specific_comment ) {
			$classes[] = 'qodef-comment--no-avatar';
			$classes[] = 'qodef-comment--' . esc_attr( $comment->comment_type );
		}
		?>
    <li class="qodef-comment-item qodef-e <?php echo esc_attr( implode( ' ', $classes ) ); ?>">
        <div id="comment-<?php comment_ID(); ?>" class="qodef-e-inner">
			<?php if ( ! $is_specific_comment ) { ?>
				<?php echo get_avatar( $comment, 80 ); ?>
			<?php } ?>
            <div class="qodef-e-content">
                <h5 class="qodef-e-title vcard"><?php echo sprintf( '<span class="fn">%s%s</span>', $is_specific_comment ? sprintf( '%s: ', esc_attr( ucwords( $comment->comment_type ) ) ) : '', get_comment_author_link() ); ?></h5>
                <div class="qodef-e-date commentmetadata">
                    <a href="<?php echo esc_url( get_comment_link( $comment, $args ) ); ?>"><?php comment_time( get_option( 'date_format' ) ); ?></a>
                </div>
				<?php if ( ! $is_specific_comment ) { ?>
                    <p class="qodef-e-text"><?php echo wp_kses_post( get_comment_text() ); ?></p>
				<?php } ?>
                <div class="qodef-e-links">
					<?php
					comment_reply_link( array_merge( $args, array(
						'reply_text' => mildhill_get_icon( 'arrow_back', 'elegant-icons', esc_html__( 'Reply', 'mildhill' ) ),
						'depth'      => $depth,
						'max_depth'  => $args['max_depth']
					) ) );

					edit_comment_link( mildhill_get_icon( 'icon_pencil-edit', 'elegant-icons', esc_html__( 'Edit', 'mildhill' ) ) ); ?>
                </div>
            </div>
        </div>
		<?php //li tag will be closed by WordPress after looping through child elements ?>
		<?php
	}
}

if ( ! function_exists( 'mildhill_comment_form_args' ) ) {
	/**
	 * Function that define new comment form args in order to override default wordpress comment form
	 * Args definition is separated into function, since it will be used for wordpress comments as well as for woocommerce reviews
	 *
	 * @return array
	 */
	function mildhill_comment_form_args() {
		$commenter     = wp_get_current_commenter();
		$required      = get_option( 'require_name_email' );
		$aria_required = ( $required ? " aria-required='true'" : '' );

		$args = array(
			'title_reply_before'   => '<h4 id="reply-title" class="comment-reply-title">',
			'title_reply_after'    => '</h4>',
			'comment_notes_before' => '',
			'fields'               => array(
				'author' => '<input type="text" id="author" name="author" placeholder="' . esc_attr__( 'Name', 'mildhill' ) . '" value="' . esc_attr( $commenter['comment_author'] ) . '" ' . $aria_required . '>',
				'email'  => '<input type="text" id="email" name="email" placeholder="' . esc_attr__( 'Email', 'mildhill' ) . '" value="' . esc_attr( $commenter['comment_author_email'] ) . '" ' . $aria_required . '>',
				'url'    => '<input type="text" id="url" name="url" placeholder="' . esc_attr__( 'Website', 'mildhill' ) . '" value="' . esc_attr( $commenter['comment_author_url'] ) . '">',
			),
			'comment_field'        => '<textarea id="comment" name="comment" placeholder="' . esc_attr__( 'Comment', 'mildhill' ) . '" rows="8" aria-required="true"></textarea>',
			'submit_button'        => '<button name="%1$s" type="submit" id="%2$s" class="%3$s" value="%4$s"><span class="qodef-m-text">%4$s</span></button>',
			'label_submit'         => esc_html__( 'Submit', 'mildhill' ),
			'class_submit'         => 'qodef-button qodef-layout--filled qodef-width--full',
		);

		return $args;
	}
}

if ( ! function_exists( 'mildhill_comment_form_fields' ) ) {
	/**
	 * Function that override default wordpress comment form fields
	 *
	 * @param $args array
	 *
	 * @return array
	 *
	 * @see mildhill_comment_form_args()
	 * @see mildhill/inc/comments/templates/comments.php
	 */
	function mildhill_comment_form_fields( $args ) {
		$args = array_merge( $args, mildhill_comment_form_args() );

		return $args;
	}

	add_filter( 'mildhill_filter_comment_form_args', 'mildhill_comment_form_fields' );
}