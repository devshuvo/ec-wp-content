<?php if ( get_the_posts_pagination() !== '' ): ?>

    <div class="qodef-m-pagination qodef--wp">
		<?php
		// Load posts pagination (in order to override template use navigation_markup_template filter hook)
		the_posts_pagination( array(
			'prev_text' => mildhill_get_icon( 'arrow_carrot-left', 'elegant-icons', esc_html__( '<', 'mildhill' ) ),
			'next_text' => mildhill_get_icon( 'arrow_carrot-right', 'elegant-icons', esc_html__( '>', 'mildhill' ) ),
		) ); ?>
    </div>

<?php endif; ?>