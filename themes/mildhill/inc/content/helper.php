<?php

if ( ! function_exists( 'mildhill_get_page_wrapper_classes' ) ) {
	/**
	 * Function that return classes for the page wrapper div from header.php
	 *
	 * @return string
	 */
	function mildhill_get_page_wrapper_classes() {
		return apply_filters( 'mildhill_filter_page_wrapper_classes', '' );
	}
}

if ( ! function_exists( 'mildhill_get_page_wrapper_outer_classes' ) ) {
    /**
     * Function that return classes for the page wrapper outer div from header.php for boxed layout
     *
     * @return string
     */
    function mildhill_get_page_wrapper_outer_classes() {
        return apply_filters( 'mildhill_filter_page_wrapper_outer_classes', '' );
    }
}

if ( ! function_exists( 'mildhill_filter_page_inner_wrap_classes' ) ) {
    /**
     * Function that return classes for the page inner wrapper div from header.php for ripped line
     *
     * @return string
     */
    function mildhill_filter_page_inner_wrap_classes() {
        return apply_filters( 'mildhill_filter_page_inner_wrap_classes', '' );
    }
}

if ( ! function_exists( 'mildhill_get_page_inner_classes' ) ) {
	/**
	 * Function that return classes for the page inner div from header.php
	 *
	 * @return string
	 */
	function mildhill_get_page_inner_classes() {
		$classes = 'qodef-content-grid';
		
		if ( is_page_template( 'page-full-width.php' ) ) {
			$classes = 'qodef-content-full-width';
		}
		
		return apply_filters( 'mildhill_filter_page_inner_classes', $classes );
	}
}

if ( ! function_exists( 'mildhill_get_grid_gutter_classes' ) ) {
	/**
	 * Function that returns grid gutter classes
	 *
	 * @return string
	 */
	function mildhill_get_grid_gutter_classes() {
		return apply_filters( 'mildhill_filter_grid_gutter_classes', '' );
	}
}