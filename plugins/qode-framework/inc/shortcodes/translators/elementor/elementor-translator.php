<?php

class ElementorTranslator {

	public function __construct() {

		$this->add_shortcodes();

		add_action( 'elementor/elements/categories_registered' , array( $this, 'add_elementor_widget_category'));
		add_action( 'elementor/editor/before_enqueue_scripts', array( $this, 'enqueue_scripts' ) );
		add_action( 'elementor/editor/before_enqueue_scripts', array( $this, 'add_inline_style' ) );
	}

	public function add_shortcodes() {
		$shortcodes = qode_framework_get_framework_root()->get_shortcodes()->get_shortcodes();

		if ( ! empty( $shortcodes ) && is_array( $shortcodes ) ) {
			ksort( $shortcodes );

			foreach ( $shortcodes as $key => $shortcode ) {

				$this->map_shortcodes( $key, $shortcode );
			}
		}
	}

	public function map_shortcodes( $key, $shortcode ) {

		if(!$shortcode->get_is_child_shortcode()) {
			$object = new QodeFrameworkElementor();
			$object->set_name($key);
			$object->set_title($shortcode->get_name());
			$object->set_controls($this->generate_option_params($shortcode));
			\Elementor\Plugin::instance()->widgets_manager->register_widget_type($object);
		}
	}

	public function generate_option_params( $shortcode ) {

		$shortcode_options = $shortcode->get_options();

		$formatted_options = array();

		if($shortcode->get_is_parent_shortcode()){

			$children = $shortcode->get_child_elements();

			foreach ($children as $child) {
				$child_object = qode_framework_get_framework_root()->get_shortcodes()->get_shortcode($child);

				$shortcode_options['elements_of_child_widget'] = array(
					'field_type' => 'repeater',
					'name'       => 'elements_of_child_widget',
					'title'      => $child_object->get_name(),
					'items' 	=> array()
				);

				foreach ($child_object->get_options() as $child_option_key => $child_option) {

					$visibility = isset($child_option['visibility']) ? $child_option['visibility'] : array();
					if (!isset($visibility['map_for_page_builder']) || (isset($visibility['map_for_page_builder']) && $visibility['map_for_page_builder'] === true)) {
						$shortcode_options['elements_of_child_widget']['items'][] = $child_option;
					}
				}

				if($child_object->get_is_parent_shortcode()){
					$shortcode_options['elements_of_child_widget']['items'][] = array(
						'field_type'    => 'html',
						'name'          => 'content',
						'title'         => esc_html__( 'Content', 'qode-framework' )
					);
				}

			}

		}

		foreach ($shortcode_options as $option_key => $option) {


			$formatted_options = array_merge_recursive($formatted_options, $this->generate_options_array($option_key, $option));

		}



		return $formatted_options;
	}


	function generate_options_array($option_key, $option){


		$formatted_options = array();

			/*** Visibility Options ***/

			$visibility = isset($option['visibility']) ? $option['visibility'] : array();
			$group		= isset($option['group']) ? str_replace(' ', '-', strtolower($option['group'])) : 'general';



			if (!isset($visibility['map_for_page_builder']) || (isset($visibility['map_for_page_builder']) && $visibility['map_for_page_builder'] === true)) {


				$formatted_options[$group]['fields'][$option_key]['field_type'] = $option['field_type'];
				$formatted_options[$group]['fields'][$option_key]['label'] = $option['title'];


				if(isset($option['default_value'])){
					$formatted_options[$group]['fields'][$option_key]['default'] = $option['default_value'];
				}
				if(isset($option['options'])){
					$formatted_options[$group]['fields'][$option_key]['options'] = $option['options'];

					if(!isset($option['default_value']) && !empty(key($option['options']))){
						$formatted_options[$group]['fields'][$option_key]['default'] = key($option['options']);
					}
				}
				if(isset($option['description'])){
					$formatted_options[$group]['fields'][$option_key]['description'] = $option['description'];
				}
				if(isset($option['multiple'])){
					$formatted_options[$group]['fields'][$option_key]['multiple'] = $option['multiple'];
				}


				/*** Dependency Options ***/

				if (isset($option['dependency'])) {
					if (isset($option['dependency']['show'])) {

						$dependency = $option['dependency']['show'];

						$dependency_key = key($dependency);

						if ($dependency[$dependency_key]['values'] === '') {
							$option['condition'] = array(
								$dependency_key => array('')
							);
						} else {
							$option['condition'] = array(
								$dependency_key => $dependency[$dependency_key]['values']
							);
						}
						$formatted_options[$group]['fields'][$option_key]['condition'] = $option['condition'];
					}

					if ( isset( $option['dependency']['hide'] ) ) {
						$dependency = $option['dependency']['hide'];

						$dependency_key = key( $dependency );
						if ($dependency[$dependency_key]['values'] === '') {
							$option['condition'] = array(
								$dependency_key . '!' => array('')
							);
						} else {
							$option['condition'] = array(
								$dependency_key . '!' => $dependency[$dependency_key]['values']
							);
						}
						$formatted_options[$group]['fields'][$option_key]['condition'] = $option['condition'];
					}
				}

				/*** Repeater Options ***/
				if($option['field_type'] === 'repeater'){

					$formatted_options[$group]['fields'][$option_key]['title_field'] = esc_html__('Item', 'qode-framework');

					foreach ($option['items'] as $item_key => $item_value) {

						$formatted_options[$group]['fields'][$option_key]['items'][$item_value['name']]['label'] = $item_value['title'];
						$formatted_options[$group]['fields'][$option_key]['items'][$item_value['name']]['field_type'] = $item_value['field_type'];


						if(isset($item_value['default_value'])){
							$formatted_options[$group]['fields'][$option_key]['items'][$item_value['name']]['default'] = $item_value['default_value'];
						}
						if(isset($item_value['options'])){
							$formatted_options[$group]['fields'][$option_key]['items'][$item_value['name']]['options'] = $item_value['options'];
						}
						if(isset($item_value['description'])){
							$formatted_options[$group]['fields'][$option_key]['items'][$item_value['name']]['description'] = $item_value['description'];
						}

						if (isset($item_value['dependency'])) {
							if (isset($item_value['dependency']['show'])) {

								$dependency = $item_value['dependency']['show'];

								$dependency_key = key($dependency);

								if ($dependency[$dependency_key]['values'] === '') {
									$item_value['condition'] = array(
										$dependency_key => array('')
									);
								} else {
									$item_value['condition'] = array(
										$dependency_key => $dependency[$dependency_key]['values']
									);
								}
								$formatted_options[$group]['fields'][$option_key]['items'][$item_value['name']]['condition'] = $item_value['condition'];
							}

							if ( isset( $item_value['dependency']['hide'] ) ) {
								$dependency = $item_value['dependency']['hide'];

								$dependency_key = key( $dependency );
								if ($dependency[$dependency_key]['values'] === '') {
									$item_value['condition'] = array(
										$dependency_key . '!' => array('')
									);
								} else {
									$item_value['condition'] = array(
										$dependency_key . '!' => $dependency[$dependency_key]['values']
									);
								}
								$formatted_options[$group]['fields'][$option_key]['items'][$item_value['name']]['condition'] = $item_value['condition'];
							}
						}

					}

				}

			}

			return $formatted_options;

	}

	public function enqueue_scripts() {
		wp_enqueue_style( 'qode-framework-elementor', QODE_FRAMEWORK_SHORTCODES_URL_PATH . '/translators/elementor/assets/css/elementor.css');
	}

	function add_inline_style() {
		$shortcodes = qode_framework_get_framework_root()->get_shortcodes()->get_shortcodes();
		$style      = apply_filters( 'qode_framework_filter_add_elementor_inline_style', $style = '' );

		if ( ! empty( $shortcodes ) && is_array( $shortcodes ) ) {
			ksort( $shortcodes );

			foreach ( $shortcodes as $key => $shortcode ) {
				$shortcode_path = $shortcode->get_shortcode_path();

				if ( isset( $shortcode_path ) && ! empty( $shortcode_path ) ) {
					$icon      = $shortcode->get_is_child_shortcode() ? 'dashboard_child_icon' : 'dashboard_icon';
					$icon_path = $shortcode_path . '/assets/img/' . esc_attr( $icon ) . '.png';

					$style .= '.qodef-custom-elementor-icon.' . str_replace('_', '-', $key) . '{
						background-image: url("' . $icon_path . '") !important;
					}';
				}
			}
		}

		if ( ! empty( $style ) ) {
			wp_add_inline_style( 'qode-framework-elementor', $style );
		}
	}

	function add_elementor_widget_category($elements_manager) {

		$elements_manager->add_category(
			'qode',
			[
				'title' => esc_html__('Qode', 'qode-framework'),
				'icon' => 'fa fa-plug',
			]
		);
	}
}

if ( ! function_exists( 'qode_framework_init_elementor_translator' ) ) {
	function qode_framework_init_elementor_translator() {
		if ( qode_framework_is_installed( 'elementor' ) ) {
			include QODE_FRAMEWORK_INC_PATH . '/shortcodes/translators/elementor/elementor-translator-class.php';
			
			new ElementorTranslator();
		}
	}

	add_action( 'init', 'qode_framework_init_elementor_translator', 99 );
}