<?php
// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

// BEGIN ENQUEUE PARENT ACTION
// AUTO GENERATED - Do not modify or remove comment markers above or below:

if ( !function_exists( 'chld_thm_cfg_locale_css' ) ):
    function chld_thm_cfg_locale_css( $uri ){
        if ( empty( $uri ) && is_rtl() && file_exists( get_template_directory() . '/rtl.css' ) )
            $uri = get_template_directory_uri() . '/rtl.css';
        return $uri;
    }
endif;
add_filter( 'locale_stylesheet_uri', 'chld_thm_cfg_locale_css' );

// END ENQUEUE PARENT ACTION

add_action( 'wp_enqueue_scripts', 'mildhill_child_enqueue_styles' );
function mildhill_child_enqueue_styles() {
	$ver = current_time( 'timestamp' );

	wp_enqueue_style( 'mildhill-style', get_template_directory_uri() . '/style.css' );
	wp_enqueue_style( 'mildhill-child-style', get_stylesheet_directory_uri() . '/style.css', array('mildhill-style'), $ver );

	wp_enqueue_script( 'mildhill-child-scripts', get_stylesheet_directory_uri() . '/assets/js/child-scripts.js', array('jquery'), $ver );
}

/**
 * Before add to cart
 *
 */
function mildhill_before_add_to_cart_btn_function(){

	global $product;
	$id = $product->get_id();

	ob_start(); ?>
	<div class="qodef-quantity-buttons quantity qodef-woo-page">
		<label class="screen-reader-text" for="quantity_<?php esc_attr_e( $id ); ?>">Lingonberry quantity</label>
        <span class="qodef-quantity-minus"><span class="arrow_carrot-down"></span></span>
        <input type="text" id="quantity_<?php esc_attr_e( $id ); ?>" class="input-text qty text qodef-quantity-input dev-quantity-input" data-step="1" data-min="1" data-max="" name="quantity" value="1" title="Qty" size="4" inputmode="numeric">
        <span class="qodef-quantity-plus"><span class="arrow_carrot-up"></span></span>
	</div>

	<?php $output = ob_get_clean();
	echo $output;
}
add_action( 'mildhill_before_add_to_cart_btn', 'mildhill_before_add_to_cart_btn_function' );


function so_38878702_remove_hook(){
	if ( is_woocommerce() ) {
		remove_action( 'mildhill_action_page_content_bottom_template', 'mildhill_core_load_page_content_bottom', 10 );
	}

}
add_action( 'mildhill_action_page_content_bottom_template', 'so_38878702_remove_hook', 1 );


function dev_search_by_cat_shortcode_function( $atts ){

    $catargs = array(
        'show_option_all'   => '',
        'show_option_none'  => 'Categoría',
        'orderby'           => 'id',
        'order'             => 'ASC',
        'show_count'        => 1,
        'hide_empty'        => 1,
        'child_of'          => 0,
        'exclude'           => '',
        'echo'              => 1,
        'selected'          => 0,
        'hierarchical'      => 0,
        'name'              => 'ixwpst[product_cat][]',
        'id'                => 'product_cat',
        'class'             => 'product_cat',
        'depth'             => 0,
        'tab_index'         => 0,
        'taxonomy'          => 'product_cat',
        'hide_if_empty'     => false,
        'option_none_value' => -1,
        'value_field'       => 'term_id',
        'required'          => true,
    );

    $attrargs = array(
        'show_option_all'   => '',
        'show_option_none'  => 'Dieta',
        'orderby'           => 'id',
        'order'             => 'ASC',
        'show_count'        => 1,
        'hide_empty'        => 1,
        'child_of'          => 0,
        'exclude'           => '',
        'echo'              => 1,
        'selected'          => 0,
        'hierarchical'      => 0,
        'name'              => 'ixwpst[pa_dietary-preference][]',
        'id'                => 'pa_dietary-preference',
        'class'             => 'pa_dietary-preference',
        'depth'             => 0,
        'tab_index'         => 0,
        'taxonomy'          => 'pa_dietary-preference',
        'hide_if_empty'     => false,
        'option_none_value' => -1,
        'value_field'       => 'term_id',
        'required'          => true,
    );

	ob_start();
	?>
	<form class="search_by_cat-wrap" action="<?php echo get_permalink( wc_get_page_id( 'shop' ) ); ?>" method="get">
		<div class="sbc_inner">
			<div class="sbc-col sbc-col-cat">
				<div class="col-inner">
					<?php wp_dropdown_categories( $catargs ); ?>
				</div>
			</div>
			<div class="sbc-col sbc-col-attr">
				<div class="col-inner">
					<?php wp_dropdown_categories( $attrargs ); ?>
				</div>
			</div>
			<div class="sbc-col sbc-col-search">
				<div class="col-inner">
					<div class="search-string">
						<input type="text" name="ixwpss" id="search-field" placeholder="Buscador...">
						<button type="submit"><?php sblfwoo_svg('search'); ?></button>
					</div>
				</div>
			</div>
		</div>
	</form>

	<?php
	$output = ob_get_clean();
	return $output;

}
add_shortcode( 'dev_search_by_cat', 'dev_search_by_cat_shortcode_function' );


function shop_rev_slider_action(){
	if( is_shop() || is_product_category() ){
		echo do_shortcode( '[rev_slider alias="shop-page"][/rev_slider]' );

		echo '
		<style>
		.qodef-page-title {
		    display: none;
		}
		#qodef-page-outer { margin-top: -105px;}

		@media only screen and (max-width: 1024px){

			.qodef-grid.qodef-layout--template>.qodef-grid-inner{
				display: flex;
			    display: -ms-flexbox;
			    display: flex;
			    -ms-flex-wrap: wrap;
			    flex-wrap: wrap;
			}

			.qodef-grid.qodef-layout--template>.qodef-grid-inner>.qodef-grid-item.qodef-col-push--3 {
				order:2;
			}

			.qodef-grid.qodef-layout--template>.qodef-grid-inner>.qodef-grid-item.qodef-col-pull--9 {
				order:1;
			}

			#qodef-page-inner {
			    padding: 0px 0 100px;
			}
		}


		</style>
		';


	}

}
add_action( 'mildhill_action_before_page_inner', 'shop_rev_slider_action', 10);
//add_action( 'woocommerce_before_main_content', 'shop_rev_slider_action', 10);


function dev_add_body_class( $classes ) {
	// Adds a class of hfeed to non-singular pages.
	if ( is_shop() || is_product_category() ) {
		$classes[] = 'qodef-header--light';
	}

	return $classes;
}
add_filter( 'body_class', 'dev_add_body_class', 9999 );