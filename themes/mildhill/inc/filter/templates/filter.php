<?php if ( isset( $enable_filter ) && $enable_filter === 'yes' ) : ?>
	<?php $filter_items = mildhill_get_filter_items( $params ); ?>

    <div class="qodef-m-filter">
		<?php if ( ! empty( $filter_items ) ) : ?>
            <div class="qodef-m-filter-items">
                <a class="qodef-m-filter-item qodef--active" href="#"
                        data-taxonomy="<?php echo esc_attr( $taxonomy_filter ); ?>" data-filter="*">
					<?php if ( $taxonomy_filter === 'product_cat' ) : ?>
                        <div class="qodef-m-filter-item-image">
                            <img alt="<?php esc_attr_e( 'All Items', 'mildhill' ); ?>"
                                    src="<?php echo MILDHILL_ASSETS_ROOT . '/img/all-items.png'; ?>">
                        </div>
					<?php endif; ?>
                    <span class="qodef-m-filter-item-name">
						<?php esc_html_e( 'Show All', 'mildhill' ) ?>
                    </span>
                </a>
				<?php foreach ( $filter_items as $item ) : ?>
					<?php $remove_category_from_filter = get_term_meta( $item->term_id, 'remove_category_from_filter', true );
					if ( $remove_category_from_filter !== 'yes' ):
						?>
                        <a class="qodef-m-filter-item" href="#"
                                data-taxonomy="<?php echo esc_attr( $taxonomy_filter ); ?>"
                                data-filter="<?php echo esc_attr( $item->slug ); ?>">
							<?php if ( $taxonomy_filter === 'product_cat' ) : ?>
                                <div class="qodef-m-filter-item-image">
									<?php

									$thumb_id = get_term_meta( $item->term_id, 'thumbnail_id', true );
									echo wp_get_attachment_image( $thumb_id, 'thumbnail' );

									?>
                                </div>
							<?php endif; ?>
                            <span class="qodef-m-filter-item-name"><?php echo esc_html( $item->name ); ?></span>
                        </a>
					<?php endif; ?>
				<?php endforeach; ?>
            </div>
		<?php endif; ?>
    </div>

<?php endif; ?>