<?php

class QodeFrameworkElementor extends \Elementor\Widget_Base{

	protected $name;
	protected $title;
	protected $widget_type = array();
	protected $controls = array();


	function __construct(array $data = [], $args = null) {
		parent::__construct($data, $args);

		$this->widget_type = $data;
	}

	public function set_name($name) {
		$this->name = $name;
	}

	public function get_name() {
		return $this->name;
	}

	public function set_title($title) {
		$this->title = $title;
	}

	public function get_title() {
		return $this->title;

	}

	public function get_icon() {
		return 'qodef-custom-elementor-icon ' .  str_replace('_', '-', $this->name);
	}

	public function set_controls($controls) {
		$this->controls = $controls;
	}

	public function get_categories() {
		return [ 'qode' ];
	}

	protected function _register_controls() {

		foreach ($this->controls as $control_key => $control) {

			$this->start_controls_section(
				$control_key,
				[
					'label' => ucwords(str_replace('-', ' ', $control_key)),
					'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
				]
			);

			foreach ($control['fields'] as $field_key => $field) {

				if(isset($field['field_type']) && $field['field_type'] == 'repeater'){

					$repeater = new \Elementor\Repeater();

					foreach ($field['items'] as $item_key => $item) {

						$item['type'] = $this->convert_options_types_to_wpb_types($item);
						$repeater->add_control(
							$item_key,
							$item
						);
					}
					$field['fields'] = $repeater->get_controls();
					unset($field['items']);
				}

				$field['type'] = $this->convert_options_types_to_wpb_types($field);
				$this->add_control(
					$field_key,
					$field
				);
			}

			$this->end_controls_section();


		}

	}

	protected function render(){

		$params = $this->get_settings_for_display();

		$object = qode_framework_get_framework_root()->get_shortcodes()->get_shortcode($this->widget_type['widgetType']);

		$params = $this->format_params($params, $object);

		// Handle neasted shortcodes
		if(isset($params['content']) && isset($params['elements_of_child_widget'])){
			echo $object->render($params, $params['content']);
		} else {
			echo $object->render($params);
		}


	}

	public function convert_options_types_to_wpb_types( $option ) {
		$type = $option['field_type'];

		switch ( $type ) :
			case 'text':
				$elementor_type = \Elementor\Controls_Manager::TEXT;
				break;
			case 'textarea':
				$elementor_type = \Elementor\Controls_Manager::TEXTAREA;
				break;
			case 'html':
				$elementor_type = \Elementor\Controls_Manager::WYSIWYG;
				break;
			case 'select':
				$elementor_type = \Elementor\Controls_Manager::SELECT;
				break;
			case 'checkbox':
				$elementor_type = \Elementor\Controls_Manager::SWITCHER;
				break;
			case 'color':
				$elementor_type = \Elementor\Controls_Manager::COLOR;
				break;
			case 'hidden':
				$elementor_type = \Elementor\Controls_Manager::HIDDEN;
				break;
			case 'image':
				if ( isset( $option['multiple'] ) && $option['multiple'] == 'yes' ) {
					$elementor_type = \Elementor\Controls_Manager::GALLERY;
				} else {
					$elementor_type = \Elementor\Controls_Manager::MEDIA;
				}
				break;
			case 'iconpack':
				$elementor_type = \Elementor\Controls_Manager::SELECT;
				break;
			case 'icon':
				$elementor_type = \Elementor\Controls_Manager::SELECT;
				break;
			case 'date':
				$elementor_type = \Elementor\Controls_Manager::DATE_TIME;
				break;
			case 'repeater':
				$elementor_type = \Elementor\Controls_Manager::REPEATER;
				break;
			default:
				$elementor_type = \Elementor\Controls_Manager::TEXT;
				break;
		endswitch;

		return $elementor_type;
	}

	public function format_params($params, $object){
		$image_params	= $object->get_options_key_by_type('image');

		if(is_array($image_params) && count($image_params) > 0){

			foreach ($image_params as $image_param){
				if( ! empty( $params[$image_param] ) ){
					$option = $object->get_option($image_param);
					if(isset($option['multiple']) && $option['multiple'] === 'yes'){
						$gallery_array = array();
						foreach ($params[$image_param] as $gallery_item_key => $gallery_item) {
							$gallery_array[] = $gallery_item['id'];
						}
						$params[$image_param] = implode(',', $gallery_array);
					} else {
						$params[$image_param] = $params[$image_param]['id'];
					}
				}
			}
		}

		$repeater_params	= $object->get_options_key_by_type('repeater');

		if(is_array($repeater_params) && count($repeater_params) > 0){

				foreach ($repeater_params as $repeater_param) {

					if (!empty($params[$repeater_param])) {
						$option = $object->get_option($repeater_param);

						foreach ($option['items'] as $item_key => $item) {

							if ($item['field_type'] == 'image') {

								if (!isset($item['multiple']) || (isset($item['multiple']) && $item['multiple'] != 'multiple')) {
									foreach ($params[$repeater_param] as $repeater_item_key => $repeater_item) {
										$params[$repeater_param][$repeater_item_key][$item['name']] = $params[$repeater_param][$repeater_item_key][$item['name']]['id'];
									}
								}
							}
						}
					}

					$params[$repeater_param] = urlencode(json_encode($params[$repeater_param]));

				}
		}

		if(!empty($params['elements_of_child_widget'])){

			foreach ($object->get_child_elements() as $child) {
				$params['content'] = '';
				foreach ($params['elements_of_child_widget'] as $child_elements) {
					$params['content'] .= '[';
					$params['content'] .= $child;
					$params['content'] .= ' ';
					foreach ($child_elements as $child_element_key => $child_element) {
						if($child_element_key != 'content') {
							$params['content'] .= $child_element_key . '="' . $child_element . '" ';
						}

					}
					if(isset( $child_elements['content'])){
						$params['content'] .= ']' . $child_elements['content'];
					}

					$params['content'] .= '[/';
					$params['content'] .= $child;
					$params['content'] .= ']';
				}
			}
		}


		return $params;

	}
}