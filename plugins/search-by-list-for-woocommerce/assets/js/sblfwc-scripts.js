(function($){
	$(document).ready(function(){

		$(document).on( 'click','.sblfwoo-trigger, .sblfwoo-close', function( e ){
			e.preventDefault();

			$(this).closest('.sblfwoo-inner').find('.sblfwoo-container').toggleClass('active');
		});

		$(document).on( 'click','.search-product-list__remove-item', function( e ){
			e.preventDefault();

			$(this).fadeOut('fast',function(){
				$(this).remove();
			});
		});

		$(document).on( 'click','.search-product-list__input-wrapper', function( e ){
			$(this).find('.search-product-list__search-input').focus();
		});

		$('.search-product-list__search-input').keypress(function(e) {
	        if (e.keyCode === 13) {
	        	$(this).focus();
	            e.preventDefault();
	        }
	    });

		$(document).on( 'focus focusout', '.search-product-list__search-input', function( e ){
			var dataVal = $(this).val();

			if ( dataVal ) {
				$(this).before( make_keyword_tag( dataVal ) );
				$(this).val("");
			}

			generate_search_by_list_link();

		});

		function make_keyword_tag( text ){
			return '<button type="button" class="btn gray search-product-list__remove-item item-'+ slugify( text ) +'" data-value="'+ text +'"><span class="text">'+ text +'</span><span class="font-icn cross"></span></button>';
		}

		function slugify(text) {
		  return text
		    .toString()
		    .toLowerCase()
		    .normalize('NFD')
		    .trim()
		    .replace(/\s+/g, '-')
		    .replace(/[^\w\-]+/g, '')
		    .replace(/\-\-+/g, '-');
		}


    	$(document).on( 'click','.search-filter', function( e ){
    		var data_filter = $(this).attr('data-filter');

    		if(!$(this).hasClass('active')){
                $(this).addClass('active').siblings().removeClass('active');

	            var data = {
	                action: 'search_list_product_filter',
	                sblfwc_ajax_nonce: sblfwc_params.sblfwc_ajax_nonce,
	                data_filter: data_filter,
	            }

	            var ajax_loader = '<div class="sblfwc-loader"><div class="lds-dual-ring"></div></div>';

	    		$.ajax({
	    			type: 'post',
	    			url: sblfwc_params.ajax_url,
	    			data: data,
	    			beforeSend: function(data){
	    				$('#search-list-filter-ajax-result').prepend( ajax_loader );
	    			},
	    			complete: function(data){
	    				$('.sblfwc-loader').hide();
	    			},
	    			success: function(data){
	    				$('#search-list-filter-ajax-result').html( data );
	    			},
	    			error: function(data){
	    				console.log(data);
	    			},

	    		});

            }

    	});

		function generate_search_by_list_link(){
			var link = sblfwc_params.sl_url + "?sl=";

			var kyewords = [];

			$('.search-product-list__list-items button').each(function(){
				var data_val = $(this).data('value');

				if ( data_val ) {
					kyewords.push( data_val );
				}
			});

			$('.sblfwc-submit').attr('href', link+kyewords);
		}


	});
})(jQuery)