<?php

if ( ! function_exists( 'mildhill_filter_enable_page_content_bottom' ) ) {
	/**
	 * Function that check is module enabled
	 */
	function mildhill_filter_enable_page_content_bottom() {
		$is_enabled = true;

		return apply_filters( 'mildhill_filter_enable_page_content_bottom', $is_enabled );
	}
}

if ( ! function_exists( 'mildhill_get_content_bottom_area_classes' ) ) {
	/**
	 * Function that return classes for page content bottom area
	 *
	 * @return string
	 */
	function mildhill_get_content_bottom_area_classes() {

		$classes = apply_filters( 'mildhill_filter_content_bottom_area_classes', 'qodef-content-grid' );

		return $classes;
	}
}

if ( ! function_exists( 'mildhill_load_page_content_bottom' ) ) {
	/**
	 * Function which loads page template module
	 */
	function mildhill_load_page_content_bottom() {

		if ( mildhill_filter_enable_page_content_bottom() ) {

			echo apply_filters( 'mildhill_filter_content_bottom_template', mildhill_get_template_part( 'content-bottom', 'templates/content-bottom' ) );
		}
	}

	add_action( 'mildhill_action_page_content_bottom_template', 'mildhill_load_page_content_bottom' );
}
